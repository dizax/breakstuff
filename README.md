####Description:
Match 3 game on 10x10 grid. Saves field and stores moves in db.

####Requirements:
* python;  
* pygame;  
* pillow (PIL);  
* sqlite3;  

####Usage:
Run break.py.  
To swap 2 neighbour tiles drag one onto another.

