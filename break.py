# -*- coding: utf-8 -*-
# __author__ = 'zax'

import time
import datetime
from PIL import Image, ImageDraw, ImageFont
import pygame
from pygame.locals import *
from field import Field
from database import Database

if not pygame.font: print ('Warning, fonts disabled')
if not pygame.mixer: print ('Warning, sound disabled')

# Объявляем переменные
ROWS = 10
COLS = 10
CELL_SIZE = 50
CELL_SIZE = 50
WIN_WIDTH = CELL_SIZE*ROWS + 50  # Ширина создаваемого окна
WIN_HEIGHT = CELL_SIZE*COLS + 50  # Высота
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)  # Группируем ширину и высоту в одну переменную
black, white = (0, 0, 0), (255, 255, 255)
BACKGROUND_COLOR = black
COLORS = [(255, 50, 50), (0, 255, 0), (0, 255, 255), (255, 200, 0), black]


def draw(screen, field, font, img_cnt):
    screen.fill(BACKGROUND_COLOR)
    for row in xrange(ROWS):
        for col in xrange(COLS):
            pygame.draw.circle(screen, COLORS[field.color_at(row, col)],
                               (int((col + 0.5) * CELL_SIZE),
                                int((row + 0.5) * CELL_SIZE)), int(CELL_SIZE / 2 - 5))
            if field.color_at(row, col) > -1:
                text = font.render(str(field.id_at(row, col)), 1, black)
                text_pos = text.get_rect(centerx=int((col + 0.5) * CELL_SIZE),
                                         centery=int((row + 0.5) * CELL_SIZE))
                screen.blit(text, text_pos)
    text = font.render("Collapsed all: %d; Collapsed now: %d; Moves: %d"
                       % (field.collapsed_cnt, len(field.ids_collapsed), field.moves_cnt),
                       1, white)
    screen.blit(text, (25, WIN_HEIGHT-35))

    w, h = font.size("99")
    for i, c in enumerate(field.ids_collapsed):
        text = font.render(str(c), 1, white)
        screen.blit(text, (WIN_WIDTH-35, 10+(2+h)*i))

    pygame.display.update()  # обновление и вывод всех изменений на экран
    # pygame.image.save(pygame.display.get_surface(), 'imgPYG' + str(img_cnt) + '.jpg')


def main():
    # Initialize pygame
    pygame.init()  # Инициация PyGame, обязательная строчка
    screen = pygame.display.set_mode(DISPLAY)  # Создаем окошко
    pygame.display.set_caption("Break Stuff")  # Пишем в шапку

    # Init font
    font = pygame.font.Font(None, 25)

    # Initialize db
    db = Database()
    game_id = db.new_game()

    # Initialize board
    field = Field(10, 10)
    field.generate()
    #field.save_txt('field' + str(game_id) + '.txt')
    #field.load_txt('field' + str(48) + '.txt')

    # visualize board
    screen.fill(BACKGROUND_COLOR)
    for row in xrange(ROWS):
        for col in xrange(COLS):
            pygame.draw.circle(screen, COLORS[field.color_at(row, col)],
                               (int((col+0.5)*CELL_SIZE), int((row+0.5)*CELL_SIZE)), int(CELL_SIZE/2-5))
            if field.color_at(row, col) > -1:
                text = font.render(str(field.id_at(row, col)), 1, black)
                text_pos = text.get_rect(centerx=int((col+0.5)*CELL_SIZE), centery=int((row+0.5)*CELL_SIZE))
                screen.blit(text, text_pos)
    pygame.display.update()  # обновление и вывод всех изменений на экран
    pygame.image.save(pygame.display.get_surface(), 'img' + str(game_id) + '.jpg')

    # Prepare Game Objects
    clock = pygame.time.Clock()

    # Main Loop
    going = True
    mouse_down = False
    brow1, bcol1 = -1, -1
    img_cnt = 1

    while going:
        clock.tick(60)

        # Handle Input Events
        for event in pygame.event.get():
            if event.type == QUIT:
                going = False
            elif event.type == MOUSEBUTTONDOWN:
                mouse_down = True
                x, y = pygame.mouse.get_pos()
                bcol1 = x / CELL_SIZE
                brow1 = y / CELL_SIZE
            elif event.type == MOUSEBUTTONUP:
                if mouse_down:
                    # Swap cells
                    x, y = pygame.mouse.get_pos()
                    bcol2 = x / CELL_SIZE
                    brow2 = y / CELL_SIZE
                    print (brow1, bcol1), (brow2, bcol2)

                    res = field.swap(brow1, bcol1, brow2, bcol2)

                    if res:
                        draw(screen, field, font, img_cnt)
                        img_cnt += 1

                        field.ids_collapsed[:] = []

                        while field.find_matches():
                            field.collapse_matches()
                            pygame.time.delay(500)
                            draw(screen, field, font, img_cnt)
                            img_cnt += 1

                            field.fall_down()
                            pygame.time.delay(500)
                            draw(screen, field, font, img_cnt)
                            img_cnt += 1

                            if field.fall_left():
                                pygame.time.delay(500)
                                draw(screen, field, font, img_cnt)
                                img_cnt += 1

                        start = time.time()
                        #db.add_move(game_id, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        #            len(field.ids_collapsed), field.collapsed_cnt, field.moves_cnt)
                        print "DB time:", time.time() - start
                        print "Collapsed:", field.collapsed_cnt, \
                            "Collapsed now:", len(field.ids_collapsed), "; Moves:", field.moves_cnt

                    mouse_down = False

        if field.cells_left_percentage() < 0.1:
            going = False

    pygame.quit()
    db.disconnect()


# this calls the 'main' function when this script is executed
if __name__ == '__main__':
    main()
