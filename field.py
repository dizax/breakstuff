# -*- coding: utf-8 -*-
# __author__ = 'zax'

from random import randint


class Field:
    def __init__(self, rows, cols):
        self.match_size = 3

        self.rows = rows
        self.cols = cols

        # 0 - red, 1 - green, 2 - blue, 3 - yellow
        self.colors_cnt = 4
        self.colors = [[-1]*cols for i in xrange(rows)]
        self.ids = [[r*cols+c for c in xrange(cols)] for r in xrange(rows)]
        self.ids_pos = [[int(i/cols), i%cols] for i in xrange(rows*cols)]

        self.collapsed_cnt = 0
        self.moves_cnt = 0

        self.collapse_by_cols = [[] for i in xrange(cols)]
        self.ids_collapsed = []

    def color_at(self, row, col):
        return self.colors[row][col]

    def id_at(self, row, col):
        return self.ids[row][col]

    def id_pos(self, m_id):
        return self.ids_pos[m_id]

    def cells_left_percentage(self):
        return 1. - float(self.collapsed_cnt) / self.rows / self.cols

    def print_colors(self):
        for r in xrange(self.rows):
            print str.join(' ', ['*' if self.colors[r][c] < 0 else str(self.colors[r][c]) for c in xrange(self.cols)])

    def print_collapsed(self):
        for r in xrange(self.rows):
            print str.join(' ', ['*' if r in self.collapse_by_cols[c] else '-' for c in xrange(self.cols)])

    def stats(self):
        return "Collapsed all: %d; Collapsed now: %d; Moves: %d" \
               % (self.collapsed_cnt, len(self.ids_collapsed), self.moves_cnt)

    def list_of_collapsed(self):
        return 'List of collapsed cells:\n' + '\n'.join([str(m_id) for m_id in self.ids_collapsed])

    def generate(self):
        col_matches = [[-1, 1] for i in xrange(self.cols)]  # format: [match_color, match_size]

        for r in xrange(self.rows):
            match_color = -1
            match_size = 1

            for c in xrange(self.cols):
                # change color if nessesary
                if match_size == self.match_size - 1:
                    if col_matches[c][1] == self.match_size - 1:
                        colors = [i for i in xrange(self.colors_cnt) if i != match_color and i != col_matches[c][0]]
                        self.colors[r][c] = colors[randint(0, len(colors) - 1)]

                        col_matches[c][0] = self.colors[r][c]
                        col_matches[c][1] = 1
                    else:
                        colors = [i for i in xrange(self.colors_cnt) if i != match_color]
                        self.colors[r][c] = colors[randint(0, len(colors) - 1)]

                        if self.colors[r][c] == self.colors[r - 1][c]:
                            col_matches[c][1] += 1
                        else:
                            col_matches[c][0] = self.colors[r][c]
                            col_matches[c][1] = 1

                    match_color = self.colors[r][c]
                    match_size = 1
                else:
                    if col_matches[c][1] == self.match_size - 1:
                        colors = [i for i in xrange(self.colors_cnt) if i != col_matches[c][0]]
                        self.colors[r][c] = colors[randint(0, len(colors) - 1)]

                        col_matches[c][0] = self.colors[r][c]
                        col_matches[c][1] = 1

                        if self.colors[r][c] == self.colors[r][c - 1]:
                            match_size += 1
                        else:
                            match_color = self.colors[r][c]
                            match_size = 1
                    else:
                        self.colors[r][c] = randint(0, self.colors_cnt - 1)

                        # row
                        if c == 0:
                            match_color = self.colors[r][c]
                            match_size = 1
                        else:
                            if self.colors[r][c] == self.colors[r][c - 1]:
                                match_size += 1
                            else:
                                match_color = self.colors[r][c]
                                match_size = 1
                        # col
                        if r == 0:
                            col_matches[c][0] = self.colors[r][c]
                            col_matches[c][1] = 1
                        else:
                            if self.colors[r][c] == self.colors[r - 1][c]:
                                col_matches[c][1] += 1
                            else:
                                col_matches[c][0] = self.colors[r][c]
                                col_matches[c][1] = 1

    def save_txt(self, name):
        file = open(name, 'w')
        for r in self.colors:
            file.write('\t'.join([str(c) for c in r]) + '\n')
        file.close()

    def load_txt(self, name):
        with open(name, 'r') as file:
            lines = file.readlines()

            for r, line in enumerate(lines):
                cells = line.split('\t')
                for c, cell in enumerate(cells):
                    self.colors[r][c] = int(cell)

    # make a move if valid
    def swap(self, row1, col1, row2, col2):
        # check if cells  are neighbours
        neighbours = (row1 == row2 and col1 != col2 and abs(col2-col1) <= 1) or \
                     (col1 == col2 and row1 != row2 and abs(row2-row1) <= 1)

        if neighbours:
            #print 'neighbour', (row1, col1), (row2, col2)

            # prohibit if id == -1 (not necessary)
            if self.ids[row1][col1] == -1 or self.ids[row2][col2] == -1:
                return False, ()

            self.moves_cnt += 1

            # swap colors
            tmp = self.colors[row1][col1]
            self.colors[row1][col1] = self.colors[row2][col2]
            self.colors[row2][col2] = tmp
            # swap ids positions
            tmp = self.ids_pos[self.ids[row1][col1]]
            self.ids_pos[self.ids[row1][col1]] = self.ids_pos[self.ids[row2][col2]]
            self.ids_pos[self.ids[row2][col2]] = tmp
            # swap ids
            tmp = self.ids[row1][col1]
            self.ids[row1][col1] = self.ids[row2][col2]
            self.ids[row2][col2] = tmp

            # collapse while can
            #self.seek_matches_while_can()

            return True
        else:
            #print 'not neigh', (row1, col1), (row2, col2)
            return False

    def seek_matches_while_can(self):
        self.ids_collapsed[:] = []

        while self.find_matches():
            self.collapse_matches()
            self.fall_down()
            self.fall_left()

    def find_matches(self):
        has_matches = False

        # [..., [topR, .., bottomR], ...]  (topR < bottomR)
        for c in xrange(self.cols):
            self.collapse_by_cols[c][:] = []

        # rows
        for r in xrange(self.rows):
            match_size = 1
            for c in xrange(1, self.cols):
                # avoid empty cells
                if self.colors[r][c] == -1:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for cc in xrange(c - 1 - match_size + 1, c):
                            self.collapse_by_cols[cc].append(r)
                    match_size = 0
                    continue

                # match check
                if self.colors[r][c] == self.colors[r][c-1]:
                    match_size += 1
                else:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for cc in xrange(c-1-match_size+1, c):
                            self.collapse_by_cols[cc].append(r)
                    match_size = 1

                # end of the row match check
                if c == self.cols-1 and match_size >= self.match_size:
                    has_matches = True
                    # appropriate for fall alg
                    for cc in xrange(c-match_size+1, c+1):
                        self.collapse_by_cols[cc].append(r)

        # cols
        for c in xrange(self.cols):
            match_size = 1
            for r in xrange(1, self.rows):
                # avoid empty cells
                if self.colors[r][c] == -1:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for rr in xrange(r-1-match_size+1, r):
                            # TODO avoid this check?
                            if rr not in self.collapse_by_cols[c]:
                                self.collapse_by_cols[c].append(rr)
                    continue

                # match check
                if self.colors[r][c] == self.colors[r-1][c]:
                    match_size += 1
                else:
                    if match_size >= self.match_size:
                        has_matches = True
                        # appropriate for fall alg
                        for rr in xrange(r-1-match_size+1, r):
                            # TODO avoid this check?
                            if rr not in self.collapse_by_cols[c]:
                                self.collapse_by_cols[c].append(rr)
                    match_size = 1

                # end of the col match check
                if r == self.rows-1 and match_size >= self.match_size:
                    has_matches = True
                    # appropriate for fall alg
                    for rr in xrange(r-match_size+1, r+1):
                        # TODO avoid this check?
                        if rr not in self.collapse_by_cols[c]:
                            self.collapse_by_cols[c].append(rr)

            # appropriate for fall alg
            self.collapse_by_cols[c].sort()

        return has_matches

    def collapse_matches(self):
        for c in xrange(self.cols):
            for r in self.collapse_by_cols[c]:
                self.collapsed_cnt += 1
                self.ids_collapsed.append(self.ids[r][c])

                self.ids_pos[self.ids[r][c]] = [-1, -1]
                self.ids[r][c] = -1
                self.colors[r][c] = -1

    def fall_down(self):
        # TODO implement block falling (instead of one-by-one cell falling)
        for c in xrange(self.cols):
            if not self.collapse_by_cols[c]:
                continue

            for i, r_clear in enumerate(self.collapse_by_cols[c]):
                for r in xrange(r_clear, 0, -1):
                    self.colors[r][c] = self.colors[r-1][c]
                    self.ids[r][c] = self.ids[r-1][c]
                    if self.ids[r-1][c] != -1:
                        self.ids_pos[self.ids[r-1][c]] = [r, c]
                    else:
                        break

                if i == 0:
                    self.colors[0][c] = -1
                    self.ids[0][c] = -1

    def fall_left(self):
        res = False

        for c in xrange(self.cols-2, -1, -1):
            if self.colors[self.rows-1][c] == -1:
                for cc in xrange(c, self.cols-1):
                    if self.colors[self.rows-1][cc] == self.colors[self.rows-1][cc+1] == -1:
                        break
                    res = True

                    for r in xrange(self.rows):
                        self.colors[r][cc] = self.colors[r][cc+1]
                        self.ids[r][cc] = self.ids[r][cc+1]
                        if self.ids[r][cc+1] != -1:
                            self.ids_pos[self.ids[r][cc+1]] = [r, cc]

                if self.colors[self.rows-1][self.cols-1] != -1:
                    for r in xrange(self.rows):
                        self.colors[r][self.cols-1] = -1
                        self.ids[r][self.cols-1] = -1
        return res

    def can_move(self):
        # ONLY FOR MATCHES_CNT = 3
        for r in xrange(self.rows):
            for c in xrange(self.cols):
                # avoid empty cells
                if self.colors[r][c] == -1:
                    continue

                # rows
                if c > 0:
                    if self.colors[r][c] == self.colors[r][c-1]:
                        if self.horizontal_subseq(r, c-1, c, self.colors[r][c]):
                            return True
                elif c > 1:
                    if self.colors[r][c] == self.colors[r][c-2]:
                        if self.horizontal_gap(r, c-1, self.colors[r][c]):
                            return True

                # columns
                if r > 0:
                    if self.colors[r][c] == self.colors[r][c - 1]:
                        if self.vertical_subseq(r-1, r, c, self.colors[r][c]):
                            return True
                elif r > 1:
                    if self.colors[r-2][c] == self.colors[r][c]:
                        if self.vertical_gap(r-1, c, self.colors[r][c]):
                            return True
        return False

    def cell_color_equal(self, r, c, color):
        try:
            return self.colors[r][c] == color
        except IndexError:
            return False

    def horizontal_subseq(self, r, c1, c2, color):
        return self.cell_color_equal(r-1, c1-1, color) or self.cell_color_equal(r+1, c1-1, color) or \
               self.cell_color_equal(r-1, c2+1, color) or self.cell_color_equal(r+1, c2+1, color)

    def horizontal_gap(self, r, c, color):
        return self.cell_color_equal(r-1, c, color) or self.cell_color_equal(r+1, c, color)

    def vertical_subseq(self, r1, r2, c, color):
        return self.cell_color_equal(r1-1, c-1, color) or self.cell_color_equal(r1-1, c+1, color) or \
               self.cell_color_equal(r2+1, c-1, color) or self.cell_color_equal(r2+1, c+1, color)

    def vertical_gap(self, r, c, color):
        return self.cell_color_equal(r, c-1, color) or self.cell_color_equal(r, c+1, color)
