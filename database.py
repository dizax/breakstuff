# -*- coding: utf-8 -*-
# __author__ = 'zax'

import sqlite3 as mdb


class Database:
    def __init__(self):
        # connect to db
        self.con = mdb.connect('log.db')
        self.cur = self.con.cursor()

        # create tables (if don't exist)
        # id <-> team_id
        sql = """CREATE TABLE IF NOT EXISTS teams_list (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    team_name TEXT NOT NULL,
                    team_code INTEGER NOT NULL
                )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS users_list (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    user_id INTEGER NOT NULL,
                    first_name TEXT DEFAULT "",
                    last_name TEXT DEFAULT "",
                    is_registered INTEGER NOT NULL,
                    team_id INTEGER DEFAULT -1
                )"""
        self.cur.execute(sql)

        # from_chat - bool (1/0); id <-> message_id
        sql = """CREATE TABLE IF NOT EXISTS messages_list (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    user_id INTEGER NOT NULL,
                    from_chat INTEGER NOT NULL,
                    chat_id INTEGER NOT NULL,
                    time DATETIME NOT NULL,
                    text TEXT NOT NULL,
                    is_move INTEGER NOT NULL
                )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS moves_list (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    message_id INTEGER NOT NULL,
                    cell1 INTEGER NOT NULL,
                    cell2 INTEGER NOT NULL,
                    collapsed_now INTEGER NOT NULL,
                    collapsed_all INTEGER NOT NULL,
                    moves_cnt INTEGER NOT NULL
                )"""
        self.cur.execute(sql)

        sql = """CREATE TABLE IF NOT EXISTS moves (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    game_id INTEGER NOT NULL,
                    time DATETIME NOT NULL,
                    collapsed_now INTEGER NOT NULL,
                    collapsed_all INTEGER NOT NULL,
                    moves_cnt INTEGER NOT NULL
                )"""
        self.cur.execute(sql)

    def disconnect(self):
        self.cur.close()
        self.con.close()

    def new_game(self):
        sql = "SELECT game_id FROM moves GROUP BY game_id"
        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()
            return len(results)
        except mdb.Error:
            print "Error: unable to fetch game_id"
            return 0

    def add_move(self, game_id, time, collapsed_now, collapsed_all, moves_cnt):
        sql = """INSERT INTO moves(game_id, time, collapsed_now, collapsed_all, moves_cnt)
            VALUES ('%(game_id)s', '%(time)s', '%(collapsed_now)s', '%(collapsed_all)s', '%(moves_cnt)s')
            """ % {"game_id": game_id, "time": time,
                   "collapsed_now": collapsed_now, "collapsed_all": collapsed_all, "moves_cnt": moves_cnt}
        try:
            self.cur.execute(sql)
            self.con.commit()
        except mdb.Error:
            print "cant add record to seasons table"
            self.con.rollback()

    """
    select game_id, min(moves_cnt), collapsed_all from moves where collapsed_all > 80 group by game_id;
    """
